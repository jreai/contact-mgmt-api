# contact-mgmt-api
Contact management api project


Project Name-

Contact Management - API application implementation based on Mule 4. Key use cases implemented are CRUD object operations, contact object information is stored in a cloud-hosted PostgreSQL database. The implementation is deployed into CloudHub.

Installation-

The Contact Management API application and database are already deployed and hosted in the cloud.
A REST API test client such as PostMan or AdvancedRestClient should be used for sending REST requests to the application.
Make sure that the client_id and client_secret request headers are added in the request and set to any values.

Usage-

Sample requests

GET Examples:

Get all contacts 
http://contact-api-jr.us-e2.cloudhub.io/api/contact

Get a contact resource by ID 
http://contact-api-jr.us-e2.cloudhub.io/api/contact/c000056

POST	Example:

Create a contact 
http://contact-api-jr.us-e2.cloudhub.io/api/contact

Body sample:

{
  "Identification": {
    "firstName": "John",
    "lastName": "Doe",
    "dob": "1980-10-12",
    "gender": "male",
    "title": "Manager",
  "Address": 
    [{
      "type": "home",
      "number": "123",
      "street": “main street",
      "unit": “100-A“,
      "city": “New York",
      "state": "NY",
      "zipcode": "12345"
    }],
    "Communication":
    [{
      "type": "email",
      "value": “johdoe@company1.com“,
      "preferred" : true
    }]
  }
}

DELETE Example:

Delete a contact 
http://contact-api-jr.us-e2.cloudhub.io/api/contact/c000056

History-

Initial version. - JR
